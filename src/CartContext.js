import React from 'react';

const CartContext = React.createContext({
  cart: {},
  setCart: () => {}
});

export const CartProvider = CartContext.Provider;
export default CartContext;