import React from 'react';

import { Fragment, useEffect, useState, useContext } from 'react';
import { Col, Card } from 'react-bootstrap';
import OrderCard from '../components/OrderCard'
import UserContext from "../UserContext";
import { Link} from 'react-router-dom';



export default function Orders() {
	// Checks to see if the mock data was captured
	
	const { user } = useContext(UserContext);

	// State that will be used to store the courses retrieved from the database
	const [orders, setOrders] = useState([]);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			
				return(

			<Col>
                <Card>
                    <Card.Body className="mb-3">

                        <Card.Subtitle>First Name:</Card.Subtitle>
                        <Card.Text>{firstName}</Card.Text>
                        <Card.Subtitle>Last Name:</Card.Subtitle>
                        <Card.Text>{lastName}</Card.Text>
                         <Card.Text>{email}</Card.Text>
                        {/*<Card.Subtitle>Product Name: </Card.Subtitle>
                        <Card.Text>{orders.map((order) => order.productName)}</Card.Text>
                        <Card.Subtitle>Quantity: </Card.Subtitle>
                        <Card.Text>{orders.map((order) => order.quantity)}</Card.Text>*/}
                        
                        
                       
                        
                    </Card.Body>
                </Card>
            </Col>

				)
			
		})

	})
	
	
}

