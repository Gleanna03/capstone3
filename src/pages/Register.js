import React, { useState, useEffect, useContext  } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardHeader,
  MDBCol,
  MDBCardBody,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsPane,
  MDBTabsContent,
  MDBIcon,
  MDBCheckbox,
  MDBInput,
  MDBBtn,
  MDBTextArea,
} from 'mdb-react-ui-kit';

export default function Register() {
	// Consume the "user" state from the "User" context object.
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields

	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ email, setEmail ] = useState("");
	const [ mobileNumber, setMobileNumber ] = useState("");
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");
	// Set to determine whether submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNumber.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNumber, password1, password2]);

	// Function to simulate user registration
	function registerUser(e) {
		
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data){

		    	Swal.fire({
		    		title: 'Duplicate email found',
		    		icon: 'error',
		    		text: 'Kindly provide another email to complete the registration.'	
		    	});

		    } else {

		    	fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
		    	    method: "POST",
		    	    headers: {
		    	        'Content-Type': 'application/json'
		    	    },
		    	    body: JSON.stringify({
		    	        firstName: firstName,
		    	        lastName: lastName,
		    	        email: email,
		    	        mobileNumber: mobileNumber,
		    	        password: password1
		    	    })
		    	})
		    	.then(res => res.json())
		    	.then(data => {

		    	    console.log(data);

		    	    if (data) {

		    	        // Clear input fields
		    	        setFirstName('');
		    	        setLastName('');
		    	        setEmail('');
		    	        setMobileNumber('');
		    	        setPassword1('');
		    	        setPassword2('');

		    	        Swal.fire({
		    	            title: 'Registration successful',
		    	            icon: 'success',
		    	            text: 'Welcome!'
		    	        });

		    	        // Allows us to redirect the user to the login page after registering for an account
						navigate("/");

		    	    } else {

		    	        Swal.fire({
		    	            title: 'Something went wrong.',
		    	            icon: 'error',
		    	            text: 'Please try again.'   
		    	        });

		    	         
		    	        
		    	    }
		    	})
		    }
		})

	}

	return (
		(user.id !== null) 
		?
		    <Navigate to="/products" />
		:
		// Invokes the authenticate function upon clicking on submit button
		<MDBContainer fluid className='mt-0 bg-front'>
      <section className='text-center text-lg-start'>
        <div className='container py-4'>
          <MDBRow className='g-0 align-items-center'>
            <MDBCol lg='6' className='mb-5 mb-lg-0'>
              <div
                className='card cascading-right'
                style={{ background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)' }}
              >
                <div className='card-body p-5 shadow-5 text-center'>
                  <h2 className='fw-bold mb-5'>Sign up now</h2>
                  <form onSubmit={(e) => registerUser(e)}>
                    <MDBRow className='mb-4'>
                      <MDBCol>
                        <MDBInput id="userFirstName" className='mb-4' type='text' label='First name' value={firstName}
		        		onChange={e => setFirstName(e.target.value)} required />
                      </MDBCol>
                      <MDBCol>
                        <MDBInput id="userLastName" className='mb-4' type="text" label='Last name' value={lastName}
		        		onChange={e => setLastName(e.target.value)} required />
                      </MDBCol>
                    </MDBRow>

                    <MDBInput id="userEmail" className='mb-4' type='email' label='Email address' value={email}
		        		onChange={e => setEmail(e.target.value)} required />

		        	<MDBInput id="userMobileNumber" className='mb-4' type='text' label='Mobile Number' value={mobileNumber}
		        		onChange={e => setMobileNumber(e.target.value)} required />

                    <MDBInput id="userPassword1" className='mb-4' type='password'  label='Password' value={password1}
		        		onChange={e => setPassword1(e.target.value)} required />

		        	<MDBInput id="userPassword2" className='mb-4' type='password'  label='Verify Password' value={password2}
		        		onChange={e => setPassword2(e.target.value)} required />

                    {/*<MDBRow className='mb-4 justify-content-center'>
                      <MDBCol md='6' className='d-flex justify-content-center'>
                        <MDBCheckbox className=' mb-3 mb-md-0' defaultChecked label=' Subscribe to our newsletter' />
                      </MDBCol>
                    </MDBRow>*/}

                    {isActive
                    	?
                    	<MDBBtn type='submit' block className='mb-4'>
                      	Sign up
                    	</MDBBtn>
                    	:
                    	<MDBBtn type='submit' block className='mb-4' disabled>
	                     Sign up
	                    </MDBBtn>
                    }

                    

                    <div className='text-center'>
                      {/*<p>or sign up with:</p>
                      <MDBBtn color='link' type='button' floating className='mx-1'>
                        <MDBIcon fab icon='facebook-f' />
                      </MDBBtn>

                      <MDBBtn color='link' type='button' floating className='mx-1'>
                        <MDBIcon fab icon='google' />
                      </MDBBtn>

                      <MDBBtn color='link' type='button' floating className='mx-1'>
                        <MDBIcon fab icon='twitter' />
                      </MDBBtn>

                      <MDBBtn color='link' type='button' floating className='mx-1'>
                        <MDBIcon fab icon='github' />
                      </MDBBtn>*/}
                    </div>
                  </form>
                </div>
              </div>
            </MDBCol>

            <MDBCol lg='6' className='mb-5 mb-lg-0'>
              <img
                src='https://mdbootstrap.com/img/new/ecommerce/vertical/096.jpg'
                // src='https://images.pexels.com/photos/2396220/pexels-photo-2396220.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
                className='w-80 rounded-4 shadow-4'
                alt=''
              />
            </MDBCol>
          </MDBRow>
        </div>
      </section>
    </MDBContainer>
	


	)
}