import React, {  useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardHeader,
  MDBCol,
  MDBCardBody,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsPane,
  MDBTabsContent,
  MDBIcon,
  MDBCheckbox,
  MDBInput,
  MDBBtn,
  MDBTextArea,
} from 'mdb-react-ui-kit';


export default function Login() {

        // Allows us to consume the User context object and its properties to use for user validation 
        const { user, setUser} = useContext(UserContext);

        // State hooks to store the values of the input fields
        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        // State to determine whether submit button is enabled or not
        const [isActive, setIsActive] = useState(true);


        const retrieveUserDetails = (token) => {

            // The token will be sent as part of the request's header information
            // We put "Bearer" in front of the token to follow implementation standards for JWTs
            fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            });
        };


        function authenticate(e) {

            // Prevents page redirection via form submission
            e.preventDefault();

            fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                // If no user information is found, the "accessToken" property will not be available and will return undefined
                if(data.accessToken !== undefined) {

                    // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                    localStorage.setItem('token', data.accessToken);
                    retrieveUserDetails(data.accessToken);

                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome!"
                    })
                } else {
                    Swal.fire({
                        title: "Authentication failed!",
                        icon: "error",
                        text: "Check your log in credentials and try again!"
                    })
                }
            })

            
            setEmail('');
            setPassword('');

        }


        useEffect(() => {

            // Validation to enable submit button when all fields are populated and both passwords match
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);


	return (
        (user.id !==null && user.isAdmin)
        ?
        <Navigate to="/admin"/>
        :
		(user.id !== null)
        ?
        <Navigate to="/home"/>
        :
		// <Row>
		// 	<Col className="p-5 text-center">
		// 		<h1>{title}</h1>
    	// 		<p>{content}</p>
		// 		<Button as = {Link} to={destination} variant="primary">{label}</Button>
		// 	</Col>
		// </Row>

		<MDBContainer fluid className='m-0 p-0' >
		      <section className='background-radial-gradient overflow-hidden'>
		        <div className='container text-center text-lg-start my-5'>
		          <div className='row gx-lg-5 align-items-center mb-5'>
		            <div className='col-lg-6 mb-5 mb-lg-0' style={{ zIndex: 10 }}>
		              <h1 className='my-5 display-3 fw-bold ls-tight' style={{ color: 'hsl(218, 81%, 25%)' }}>
		                I dreamt about happiness! <br />
		                <span style={{ color: 'hsl(218, 81%, 55%)' }}>I am coffee dreaming.</span>
		              </h1>
		              
		            </div>

		            <div className='col-lg-6 mb-5 mb-lg-0 position-relative'>
		              <div id='radius-shape-1' className='position-absolute rounded-circle shadow-5-strong'></div>
		              <div id='radius-shape-2' className='position-absolute shadow-5-strong'></div>

		              <div className='card bg-glass'>
		                <div className='card-body px-4 py-5 px-md-5'>
		                  <Form onSubmit={(e) => authenticate(e)}>
                 
                  <MDBInput className='mb-4' 
                  controlId="userEmail"
                  type='email' 
                  placeholder="Enter email" 
                  label='Email address'
                  value={email} onChange={(e) => setEmail(e.target.value)} 
                  required />
                  
                
                  <MDBInput className='mb-4' 
                  controlId="password"
                  type='password' 
                  placeholder='Password' 
                  label='Password'
                  value={password} onChange={(e) => setPassword(e.target.value)} 
                  required  />
                   

                  <MDBRow className='mb-4'>
                    <MDBCol md='6' className='d-flex justify-content-center'>
                      <MDBCheckbox className=' mb-3 mb-md-0' defaultChecked label=' Remember me' />
                    </MDBCol>

                    <MDBCol md='6' className='d-flex justify-content-center'>
                      <a href='#!'>Forgot password?</a>
                    </MDBCol>
                  </MDBRow>


                { isActive ?
                    <MDBBtn  type='submit' block className='mb-4 mx-auto d-flex justify-content-center' color="primary" id="submitBtn">
                      Sign in
                    </MDBBtn>
                    :
                    <MDBBtn  type='submit' block className='mb-4 mx-auto d-flex justify-content-center' color="primary" id="submitBtn" disabled>
                          Sign in
                    </MDBBtn>

                }


                  <div className='text-center'>
                    <p>
                      Not a member? <a href='/register'>Register</a>
                    </p>
                    {/*<p>or sign up with:</p>
                    <MDBBtn color='link' type='button' floating className='mx-1'>
                      <MDBIcon fab icon='facebook-f' />
                    </MDBBtn>

                    <MDBBtn color='link' type='button' floating className='mx-1'>
                      <MDBIcon fab icon='google' />
                    </MDBBtn>

                    <MDBBtn color='link' type='button' floating className='mx-1'>
                      <MDBIcon fab icon='twitter' />
                    </MDBBtn>

                    <MDBBtn color='link' type='button' floating className='mx-1'>
                      <MDBIcon fab icon='github' />
                    </MDBBtn>*/}
                  </div>
                </Form>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </section>
		    </MDBContainer>
	)
}