import { Row, Col, Card, Carousel } from 'react-bootstrap';
import Typewriter from 'typewriter-effect';
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBIcon
} from 'mdb-react-ui-kit';



export default function Highlights() {
	return (






		<MDBFooter  className='  bg-front text-center text-black' style={{ backgroundColor: 'white' }}>

      <MDBContainer className='p-3 '>
        <section>

          <MDBRow className='d-flex justify-content-center'>
            <MDBCol lg='6' className="">

              <div className='ratio ratio-16x9'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Ny4bvM32i-s?autoplay=1" title="YouTube video player" frameBorder="0" allow="access-control-allow-origin; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>
              </div>
            </MDBCol>

            <MDBCol lg='6'>

              <Card className="cardHighlight p-3 text-black">

            <Card.Body>
                <Card.Title className="pb-1 ">Let's Make Our Coffee Dream Come True</Card.Title>           
                <Card.Text>
                  <Typewriter options={{
                    loop:true,
                  }}
                  onInit={(typewriter) => {
                    typewriter.changeDelay(30).typeString('"I believe humans get a lot done, not because we’re smart, but because we have thumbs so we can make coffee." Flash Rosenberg')
                    .pauseFor(2000)
                    .changeDeleteSpeed(0.2)
                    .deleteAll()
                    .changeDelay(30)
                    .typeString('"I’m going to start measuring the complexity of coding tasks in coffee cups. This was a five-espresso algorithm."')
                    .pauseFor(2000)
                    .changeDeleteSpeed(0.2)
                    .deleteAll()
                    .changeDelay(30)
                    .typeString('"Coffee is a lot more than just a drink; it’s something happening. Not as in hip, but like an event, a place to be, but not like a location, but like somewhere within yourself. It gives you time, but not actual hours or minutes, but a chance to be, like be yourself, and have a second cup." Gertrude Stein')
                    
                    .start()
                  }}
                  />
                </Card.Text>               
              </Card.Body>
            </Card>
            </MDBCol>
          </MDBRow>
        </section>
      </MDBContainer>

      {/*<section className=' mt-3 border-top'>
              <MDBContainer className='text-center text-md-start mt-5  p-0 '>
                <MDBRow className='mt-0'>
                  <MDBCol md="3" lg="4" xl="3" className='mx-auto mb-4'>
                    <h6 className='text-uppercase fw-bold mb-4'>
                      <MDBIcon icon="gem" className="me-0" />
                      Company name
                    </h6>
                    <p>
                    Cafe Of Dreams
                    </p>
                  </MDBCol>

                  <MDBCol md="2" lg="2" xl="2" className='mx-auto mb-4'>
                    <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
                    <p>
                      <a href='#!' className='text-reset'>
                        Hot Coffee
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Iced Coffee
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Frappuccino & Smoothies
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Snacks
                      </a>
                    </p>
                  </MDBCol>

                  <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
                    <h6 className='text-uppercase fw-bold mb-4'>Useful links</h6>
                    <p>
                      <a href='#!' className='text-reset'>
                        Product List
                      </a>
                    </p>
                  
                    <p>
                      <a href='#!' className='text-reset'>
                        Orders
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Help
                      </a>
                    </p>
                  </MDBCol>

                  <MDBCol md="4" lg="3" xl="3" className='mx-auto mb-md-0 mb-4'>
                    <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
                    <p>
                      <MDBIcon icon="home" className="me-2" />
                      Dasmarinas, Cavite, 4115, Ph
                    </p>
                    <p>
                      <MDBIcon icon="envelope" className="me-3" />
                      CafeOfDreams@mail.com
                    </p>
                    <p>
                      <MDBIcon icon="phone" className="me-3" /> + 63 987654321
                    </p>
                    <p>
                      <MDBIcon icon="print" className="me-3" /> + 63 912345678
                    </p>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </section>*/}


      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        © 2023 Copyright:
        <a className='text-white' href='https://mdbootstrap.com/'>
          CafeOfDreams.com
        </a>
      </div>

      
    </MDBFooter>


	)


}