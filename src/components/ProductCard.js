import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Button,Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardHeader,
  MDBCol,
  MDBCardBody,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsPane,
  MDBTabsContent,
  MDBIcon,
  MDBCheckbox,
  MDBInput,
  MDBBtn,
  MDBTextArea,
} from 'mdb-react-ui-kit';

export default function ProductCard({productProp}) {

    console.log(productProp);

    // const [cartItems, setCartItems] = useState([]);
    const {_id, name, description, size, price, productImage, product } = productProp;

    


    return (
                                        
      <MDBContainer style={{ width: '35rem'}} className="justify-content-center p-1 m-5 d-flex d-inline-flex text-center text-lg-start">
        
          <MDBRow className='g-0 align-items-center'>
            <MDBCol  className='mb-5 mb-lg-0'>
              <div
                className='card cascading-right'
                style={{ background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)' }}
              >
                <div className='card-body p-0 shadow-5'>
                  
                      <Card.Body className="mb-3 py-0">                                     
                          <Card.Title className="pb-3">{name}</Card.Title>
                          <Card.Subtitle >Description:</Card.Subtitle>
                          <Card.Text >{description}</Card.Text>
                           <Card.Subtitle>Size:</Card.Subtitle>
                          <Card.Text>{size}</Card.Text>
                          <Card.Subtitle>Price:</Card.Subtitle>
                          <Card.Text>PhP {price}</Card.Text>

                          <Button variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>
                          {/*<Button variant="primary" onClick={() => onAdd(product)}>Add To Cart</Button>*/}
                      </Card.Body>
                                                 
                </div>
              </div>
            </MDBCol>

            <MDBCol lg='6' className='mb-5 mb-lg-0'>
              <img className="pb-3" src={`${process.env.REACT_APP_API_URL}/${productImage}`}
                          alt="Uploaded Image" />
            </MDBCol>
          </MDBRow>
        
      </MDBContainer>
                  
                    
           

        
    )
}