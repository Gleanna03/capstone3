import Container from 'react-bootstrap/Container';
import  React, { Fragment, useContext, useState } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {
  NavbarBrand,
  Button,
  Modal,
  ModalHeader,
  ModalBody
} from 'reactstrap';
import {
  MDBContainer,
  MDBCol,
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarToggler,
  MDBIcon,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBNavbarLink,
  MDBBtn,
  MDBCollapse,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBBadge,
  MDBRow,
} from 'mdb-react-ui-kit';
import Cart from './cart/Cart';
import CartContext from '../CartContext';




export default function AppNavbar(){

	// State to store the user information stored in the login page.
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);
	const [showNav, setShowNav] = useState(false);
	const { cart } = useContext(CartContext);
  const [modal, setModal] = useState(false);
  const toggleModal = () => setModal(!modal);


	

	const { user } = useContext(UserContext);


	return (


	
	      <section>
	        <MDBNavbar className='py-0' expand='lg' light>
	        	
	          <MDBContainer fluid  >
	          	

	          	
	            <MDBNavbarBrand className=' p-0 ms-0 ms-lg-3 d-flex align-items-center' href='/'>
	              <MDBIcon icon='flask' className='text-primary me-2' />
	              <small className='fw-bold ms-auto '>
	              <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAD2APoDAREAAhEBAxEB/8QAHQABAAICAwEBAAAAAAAAAAAAAAcIBQYBBAkDAv/EAE4QAAEEAQMCAwMHBgkICwEAAAEAAgMEBQYHERIhCDFBExRRCSIyQmFx0hUYVoGRlBYXI1KCkqGxwSQzNUNicoPDJSY2RUZTVXOTsrPT/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMFAQQGAgf/xAA4EQEAAgECBAMFBgUEAwEAAAAAAQIDBBEFEiExBkFRExRhcZEWIjKhsdEzQlOB8BVDweEkNFJy/9oADAMBAAIRAxEAPwD1TQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEHHI+KByD5FBygICAgICAgICAgICAgICAgICAgIOveyFHGVJb2RtxVq0DeuWWV4YxjfiSewXjJkrirNrztEMxG/ZDmqPFPonETPrafoXc49h/zkZEEB+5zvnH7w3hc9qfEumwzy4om/wCUJ66a9o3no1CXxeZbrPstEU2t9A+88n+xir58V28scfVNGjj1fj87zNn/AMF0P3x/4Vj7V5J7Y4Z9zj1PzvM3+hdD98f+FPtXfzxwe5x6n53mb/Quh++P/Cn2rvt/Dg9zj1b9s5vjkN0M9dw9vAV6DalP3kPisOkLj1tbxwQPirXhHGr8Sy2x2pttG6DNg9lETEpfXQNdr2utf6O2103a1brnUNPDYqp2ks2n8DqPkxoHznvPHZrQXH0C82vFY3lJixXzW5Mcbyoput8qHkXW5sdszoiuKreWsyme6i+T/aZWjI6R8Ot/PxaFqZNX5Uh0em8PdN9Rbr6R+6Dr/j78VN6Uyt3ErVGk9o62GqNa37PnMcf2kqGdTknzWMcF0Ufy/nLJYL5RLxP4iwyW/qPB5qJpHMN7DRNDgPTqh9m4LManJHd5vwLR27RMf3WC2x+VC0xkpIsfu3oW1hXuPDsjh3m3XBJ83Qu4laP90vU1NXX+aFXqPD+SvXDbf4T0XK0TrzR+4uAr6n0PqOhmsXZ/zdmnKHt59Wu9WuHq1wBHqFtVtF43hQ5cWTDbkyRtLPr0jEESbzb2ZDa7LY7G08BXvi9WfO58th0fSWv6eAADyqDi/GbcMtWta77p8OD228o8/O8zn6FUP31/4VUfavJHfFH1Txo4nzPzvM7+hND99f8AhT7WX8scfVn3L4n53md/Qmh++v8Awp9q8nnij6nucer9R+L3MB49toim5vqG3ng/2sWY8V33/hfmx7nt5tu0z4rNFZWdtbUOMvYV7yAJTxYhH3lg6h/VVjpvEumyzy5vuz9YRW0to616plx2SoZenDkcZdht1Z29cU0Lw9jx8QR2K6HHlpmrF8c7xPo15iY6S7K9sCAgICAgICDqZbKUcLjbOWydllepTidNNK89mMaOSVHmy0wUnJedojuzEbztClW7G7Ob3Lyz+qWSthYHn3SiDwOPSST+c8/sb5D1J+b8V4nk4hk7/cjtH/Kzw4Yxx17tCBAPJIHPxVTunbdh9ptyc/Xbbxmi8nJA8cskkjELXD4j2haSPuW/h4VrM9ebHjnb6fqinNSOkyyP8Qu7fpouyP8Ajw/jU8cC4hH+3+cPPvGP1fKzsXuxVgksy6LtlkTS93RLE53A8+AHcn7gsTwPXxE2nH+cHvGP1aJxz34PZVU1ms7TCaOvVOXhJH/XjM8/+lf85i6jwr/7N/8A8/8ALV1f4I+a1E73Rxue1rnFoJ6W8cu+wc9uV3MztCv23eMnii3w3A3p3LyUutK1vE1MJbmpY7ATct/JjWu6XB7fWc8cvee58h80BVme05J2l3vDNJh02DfHO8z3lDfHCgiNljvuIwckIHJQb5s5vduHsVqhmqNAZh1dz3N99oy8uqX4x9SaPnv254cOHN8wQvdclsc71auq0WHWV5csf384euXh28QekfEPohuqNO81L9Vwgy2LlkDpqM5HPBI+lG7uWP44cAfIggWeLLGWN4cPrNHk0WTkv/afVKqlairPi5/7Uaf4H/d8v/6hcP4s/i4/lP6t7Rz92UEd/gVyndux0bzT2O3XvVYrlfRdv2UzQ9nXJExxB8iWucCP1hWePguuyVi9cc7T8kU6jH2mX2/iD3d/Qyx+8Q/jXueBcQ/p/nH7sRqMcebH5baDc3B1328jovJNhZ3c+JjZg0fE+zLjx9qiycI1uGvNbHO31Z9tS3TdqHB8x8VXzvHdJ0bztXupnds8w2avJJZxNh498ol/zXj1eweTXgeo8/I/ZacL4pk4fk333pPeEeXFXLXaO662FzOO1BiquaxNltipdibNDI3yc0/3H0I9CCvpWHNTPSMlJ3ieqqtE1nll3lIwICAgICAgr74sNYzUsZjNEVJi05BxuXAD5wsPDGn7C/k/0FynijVTXFXT1nv1n5NrS4+aZmVZOftXDxCx281l/Dns7jm4yvuDqWkyxZtfPxsEzeWQx89pi0+b3ebefIcHzPbtvD/CKRSNVnjeZ7R6K/UZ+aeWqwnAPbjuutae+4Gt+AWNoNjpbwekBJiJIjZHsmwG0csjpZNHxF8ji9x95m7knk/X+Kqp4JobTNrY+/xlNGoyR0iWY0pthofQ1+bJaXwbKNieL2Ej2zSP6mcg8cOcR5gKfS8O02ivN8Ndpl5vlveNrSgzx5b3at2U0HpTK6HyXueWt6khd87vHNXgifJJDIPrMeehrh58HtwVNqck0iOVZ8H0lNXktXJ22/yWmX9r9iPlA9Ex7maZtu0vrmvEyrk5IWtklgsBvzYrcXb28fY+zlBa4t8ndi0YmtNRXeGxXPqeC5PZX60nt/0qLuj4KfEHtdPPJY0VNqLFxckZLAg24y0fWdEB7Vn28t4+0rVvp707RuvdPxfS6jaObafSf3QZPDJWnfVsxPhnjPD4pWFkjT8C08EKHaVjWYvG8dnz8vNYZEBBJXh63uzmwO5mO11ijJLR5FbMUmuIFyi5w9ozj+e36bD6OaPQlSYr+ytu09fpI1uH2c9/L5vanAZzF6lwlDUOEust4/JVo7dWxGeWyxSNDmOH3ghWvNzRvDgL1mlprbvDDas2y0Trm1Xu6pwjL01WMxROdNIzpaTyR81w9Vp6nQ6fWbTnrvMM0yXx/glg2+HzaJjg5uj4gWnkH3qfsf661o4LoIneMf6vfvGSY2mUhNY1gDeFa1rshc/N+A/YvTG7kAefCxtDKA/ETs9j7eJta+05SZBfpj2uQiibw2xD9aTgdg9vmT6jnnuAuW4/wmmXHOpwxtaO/wAY/ds6fNNZ5J7KvEk+R9Vw0xt0WULKeE7WMs1fKaGtSkisPf6YJ+ixzumVo+zqLXf0iu18MavmrbT28usNDV0688LFLrmmICAgICDhYkU08S959vdq/C93LadWrAzv5Ax9Z/teV878R5ObX2ifKIWemjbHujOpWN23BSaeDYlZCD/vODf8VS46xa8Vnz6NiZ2rL0Px1KvjaFbHVWBkNWJkEbR5BrQAB+wL63jpGOsUr2hSzO87uwpGHKDhBygIKSfKWYaPOs2kx9yV8FG5qeWjZmb5xtmbE0uHPbkN6iPuWnq435YX/ArcntbR35VK8TqjdDwmb15aHT+QNHNaevzY63DI0ur5Cu1/LWSs5HXG9nQ8HsR1BzSD3WtzWwX6OitTBxXTRv2n6xL0t8O3jK2w33p1sXJch09q8tAmwlycAyv9TWkPAmafgOHj1b6nexZ65PPq5DW8Lz6Kd5jevrCV9Y7W7b7hRGHW2hcDnARxzfx8cz2/c4jqH6ipJpWe8NPHqMuHrjtMf3QdrD5Ozw16nL5MXgsrpmZw7OxGReGB3x9nN7Rn6gAop0+Oywxca1mL+bf5wr9r75LfVmPimtba7lUMt0gujpZiqasrv9kTRlzCfvY0fcobaTb8K0w+IaT0zV2+Sneu9vda7Y6hn0rr7Td3CZSDuYLLOBIzngPjeOWyMPo5pI/X2WrbHak7SvcOoxaisWxW3a6vCbbd6kfJqbkzas2UuaIvzuktaLyBqw9R5Ipzgywj7mu9q0fY0BWWmtzU2cbx7BGLVc8fzRut2tlSCAgICD4260FytLUsxh8U7HRSNPk5rhwR+wrzasWiaz2lmJ26vPDKUjjcrexpJPudmWv/AFHlv+C+R5ccYstqR5TMLis81YlIXhzvSUt3MQ1p+bajsVnj4gxOd/ewK28PX5NfWPXf9EOojfFK6a+kKwQEBAQEBJFM/EvRfT3avzOb0tu1Ks7O3n8zoP8Aawr5z4jpy6+0+sR+iz00744RlUsmlbgujzrysm/quDv8FSUtyXi8+U7p7RvWXohjb1bJ0K2SpyB8FuJk8Th5Oa9ocD+wr67ivGWkXr2lSz0nZ2VICAgICCsfyhmhrmqvDxcz2Lb/AJdo/I1s7G9o+cyNhMcrh/utk6/6ChzxvTePJbcFzxh1UVntbor74m9uK/iS2S034tNu6nt81WxTINVUYGh0kjYR0yydI7l8Dw/n1MRB+qOdbJWMtfaQs+H550OptosnaZ6f58VHWP4LZI3EOaQ5rgeCCPIg+h+BWp1js6RPe1fjg8Qm1kMONi1THqTFQ8BtHPMNnpaPqsm5EzR8B1ED4KamovToq9TwfS6jrttPrH7dlo9B/Kj6FvMirbj7eZjDTcAPs4uVl6Dn49DuiQD7OHFbNdVWY+8p8vh7LX+FaJ+fRZfbLxH7K7vvFbQWv8ZfukAmhI417g7f+TKGvP3gEKeuSt+0qjPotRp+uSsxD8797DaK3/0RPpTVVZsdmMOkxeTjYDPjrHHaRh9WnsHsPZw7efBGL4oydzSavJpMkXpPzj1eN+5O3eptqdb5XQGr6ggyeJm9lIW8+zmYRyyWMnzY9pDmn4Hg9wVWZKTS20u90+orqsUZadpW2+SxyM0e5GuMQ0u9jYwdWy4enVHYLR/ZI5bGkn70wo/EUROOlvPd6TqwcoICAgIPjcsw0qstyy8MhgY6WRx8mtaOSf2BeMl4x1m09oZiN52eeGTu/lPK3cnwf8ssy2O/+28u/wAV8ky39pltf1mVxWOWsQkLw50ZLm7eJkYPm1I7Fh/2AROb/e8K18P1319Z9N/0Q552xyumvpKsEBAQEBAQV+8WGjZbuKxut6kRccaTTtkDniF5BY4/YH8j+muU8T6Sb4q6isduk/Jt6XJyzNZVj8lw/dYLLeHLeLH/AJLg2/1NdZXsVf5PGTyu6WTRk8iEuPYOaezefMcDzHfteAcYxzSNLmnaY7fFX58U83NELDdQ8ie665qHI+IQOpvxTca+7cPQTXOY/WuCa5pLSDkIuQR5j6S1J1+ljvkr9YeuS0+Tu4rVOms7M+vhNQY3ISxt63sq2mSua3njkhpPA59V7xarDnnlxXiflO5NbV7w7GXxVDOYu3hsrVZapXoJK1mB45bLE9pa9p+wgkKeY3jaWImazFo7w85dAa2zvgD32zG0+um2be22o5xaq2ywvMcB+bHcY3j5xa3iKdg7noDhzw0O0on2F+Se0unzYo4xp65sX8SveP8APyfDxXeC0RQT72eHuvDmNLZKP8o28TjSJTXa8dRsVA3n2kB56ixvdnJ6QW9m4z4Ij71HrhnFYp/4+q6W9f3UpBBHIPK03SA80YfSOR8MrLEMro5YnBzJGO6XMcPItI7g/aFmJmOxMbxtK4Hhn+UE1ht/aqaQ3js2tS6ZJbFFk3kyZDHN8gXHzsxj1Dv5QDyLuOlbWLUzXatlDxDgmPNE5NP0t6eUpg+UC2q0/uttPi/EJoKetkZsFAx89um8PZdxEp+nyPpeye4OHwa6TnyUuoxxenPVocF1NtLnnS5em/5SwPyWOhLsNfXG5FmF7a1l9bC1HkdnmPqlm4P2F8Q+9Y0ldomyXxFnra1MdfnK9WV1PpzBSRw5rPY+g+UFzG2bLIi4DsSA4jkKXLqcOHaMloj5zs5uKzbtDo/xhaDJAGtMESTx/pCL8Si/1DS77e0r9YevZ3jvDPh7XDkOBH3rb3eHPI+IWQ6h5AoIB8RO8mPrYq1oDTVxti7bHsshPC/lteL60XUOxe7yIH0Rzz3K5Xj3F6Y8c6bDO8z3+Eejb0+GZnmsrD3+C4ZYd1lPCdo2WGtk9c24y1tr/IKfI82NPMrh9nUGt/oldn4X0c1rbU28+kNDVZN55IWJXYNMQEBAQEBB1cpjKWZx1nFZKsyxVtxuhmieOQ9jhwQVHkx1y0ml43ie7MTMTvCle7W0WZ2zyj5AySzhJ5D7pcA5DQfKOT+a8fsd5j1A+b8V4Tk4dfeOtJ7LPFnjJHWeqPxyfMdlURvum6NtxG6+5OBrtqYvWmSihb2ZG+QStaPgBIHcBb+DimswV5aZJiP89Uc4cc94ZA777ufV1vbP/Bg/Apo45xD+pP0j9mPd8fo+VnfDdm3XkrTa2u+zlaWODWRMJB8+C1gI+8HlYnjWvtG05J/I9hj9GjcgnuB+xVnNNvxJY6Jz8JHH8OMx2H+if+cxdT4WiK6m8R/8/wDMNXWfhj5rWLuVejDxAbA6M8Qein6U1TG6C3A502LykLAZ8fYI4628/SaeAHsPZw+BAIiyYq3jq2tHq76PJz0+nq8/9Pbi+I3wB6xOh9VY38saRtTukgqTPf7hcaD86WjPwTBIR3dGR5n5zPrLV5rYPu27OmyYdJxnH7XHO1/P/tIGe0X4OvGNK7OaB1dFttuHe5knoXI2QMtzEd+uAuEUrif9ZA8OPm4E9knkzfh6S1sWTW8L6Za89IQNuJ4HvEft5YmP8BZdSUGEll7AP97Y9vx9l2lb+tn61FbT3r2jda4eLaTPG/NtPpKE8lp/P4ad1XMYHJ0JmfSjtUpYXD7w5oKimlo7w365KXjesxP93V92stj9s6tMIh/rDG4N/rccLExMd3rePVb/AOT83P1La1Jk/D3l8Pez+jNU0rRmijYZG4lzmESSu9GQyAlrgfrlhHcu52tNO+9Z7KHjmDHFI1NJ5bxt/d6J7WbaaX2h0HiNvdH1nw4zEQCJjpD1SzPJ5fLI76z3uJcT8T8OFu0rFI5YcrnzX1GScl+8oG8XIH8J9P8AIB/6Pl8//dC4vxXbly4/lP6tnRxvWUC9LT9UfsXJ7zPVvTEN5q727rU60VStre82KFgYwOjieQB5DlzCT+sq0pxrX1rFYyT0+SC2DHPXZ9P4993f04uf/BB//Nev9d4h/U/KP2I0+PbeYY/L7tbmZys+pkta5OWF44dGx4iDh6g+zDeVDk4trssbXyTszXFSO1Wpt7nsPMqun70vct82r2pze5mXbHBHJWxFd498vFvAaPVkf86Qj08h5n4G14ZwvJxDJ22pHef2+KPLljFX4rqYXD4/AYurhsVWbXqU4mwwxt8mtH959SfUr6Vhx0w44x0jaI6Qq7Wm080u8pWBAQEBAQEBB171CnkqstK/Vis1529EsUrA9j2/Ag9iF4yY65KzW0bxJvt2Q9qfwsaFy8zrOBuXcHI5xJjiImh/Ux/dv6ncfYuf1PhvS5Z5sW9Z+sNimotXv1ahL4QskXER65rFoPYuoOB/+6r58Jz/AFPyTe+fB+B4QssPLW9P9xf+Nefspkj/AHI+jHvfwPzQsv8ApvT/AHF/41mPCmSO+SPoz758D80LLfpvT/cX/jT7KX36ZI+h758G+7ObHXtrs/dzNnUMGQbbp+6iOOuYy09bXc8lx5+jwrXhPBr8Oy2yWvvvG3ZDmz+1iI2S8r9riDBay0NpLcHT9nS2tdP0szirg4mq24w9hPo4erXD0c0hw9CvNqxaNpe8WW+C3PjnaVHd3PkvYrE0+V2Z1myuwkvZhs6HPYw+fTHZYC7j4dbHH4uWtbS+dZdFpfEE16aiu/xhEdfZz5QPZt/5O0vFrdlNhBY3CZht6q4Dy4j6zx9xYPuUU48tPwzLfnVcL1Ub323+MbSzEGo/lMc6PyXHU3DjDgYy+XEVavby7ySRt/bykTqNtuqHk4Rj+9vH1mWy6V8BXiN3byEGU8Qe5U+OpMeHurvyDslcI9QxoPsIifLnl3HwKV0179bSiy8Y02njk0td/wAoXh2h2R252P03/Brb3AsoxSFr7VmR3tLVyQDgPmlPd58+B2aOewAW5XHFI2hz2p1eXV358s7t9UjXRNvJsnd3SyuOyVbUEGPFGu+AskrukLy5/VzyHDhUPF+DW4netovttHo2MGf2O/RHv5oWX/Tin+4v/GqiPCmT+rH0S+9/A/NCy36cVP3F/wCNPspk/qx9D3uPQ/NCy36cU/3F/wCNPspk7e1j6HvnrD9R+EHJFwEuuawb6ltBxP8Aa9ZjwpftOWPoz75tHSG3aY8K2h8RM2xnr93NvYeRFIRBCfvaz5x/W5WGl8NaXB97JM2n6Qhtqb27dEw0MdRxVOKhjKcNWtC3ojhhYGMY34ADsF0GPHXHWKVjaIa9pmersjyXscrIICAgICAgICAgICAg4J4HKDq4rLYrOUIsphclVv05+r2dirM2WJ/Di09L2kg8EEHg+YIQdtAQEBB+ehvPPA5WNhyWtPm0LIAAeSDlAQEBAQEBAQEBAQEBAQEBAQY/P5aHA4S/nLLHvhx9aW1I1n0nNY0uIH2nhRZssYcdsk+Ubs1jmnZruloNf3Y8fqDM6kpiK6xs8+LjoDohY9vLWMl6usubyOXO5B4PYdlq4PeLcuS946+W36T3ercsdNmy5fLY/A4m5m8vbjq0cfXktWp5DwyKGNpc95PoA0E/qW+8K+eFffHcrcHO5zTu7talTv5fF0NeaVggr+xezT2QdI2KvIPrzV3RNbI7z5nZyEG+eKbcDUm1fh619uJo+xBBmcBhJ7tKWaESsZK3jhxYezgOT2PZB0PDvqyLVlTM2oPErgd22QvrtL8VTpVxjHOa49D/AHVzuS8dx19x0Hj1Qaf4xtx8ppGfb3Rf8Y2P0Rp/XeUyWIz2YuY9ltsdVuNnlbGGvI6faPa1nUCCOvsQglPYarj6e1Gnq+G1njdU4xlcihksbioMbVkrB7hGyKvABHGxjQGANA+jyg389gghKlmN2t2dSavfozXlLR2D0tmJdP0m/kaO/PftQMYZ5pzK4BsIe/oayPpcQxzi8cgCOJm2+zcmmPBWvPXmmY377bOzg93dTZzw66j3GnpU6ep9O47OQ2oYgZKwyOO9vG5zATyYnSQhwaTzw7gk8crNJ5o3R6rDGDJNI7dJ+sbtT8Iu5uS3Ow9HO5fxOad3CyVzTtDIZHT2Mx9GvLhrM7GPeZDXeZOzi+PpeB3HfuF7a6yKDQ8td1dlNf29OYTUkWKq08TWugOoMse0kkllYerqIIADB2BCrb3z5NTbFjvtERE9t0kREV3mGY0JqS/qPFWPyvWhgyWNvT424ICTE6WJ3HWznv0uaWuAPcc8ein0ee2es83eJmJ/s83rFZ6K7eI3fPUOit9sfoCbf7Tm1GmxoezqN2Ry+KrWxbvMvNgbD/LPaSOgk9EZD3cHj7Nt5TdsLrHWuv8AZ3SOstxdNDT+pMxiobeSxwjfGIJnDvwyT58YcOHhjvnNDuk9wUEGbvb0aix/iIzm2VnxIYra3FY/TmFv42Ozgat6TI2bc92OXh03cBnsIQAP56C1lVk8NaOOzY9vKxjQ+XpDetwHd3A7Dk9+PTlBWrUviF1zi/EpTxFX3QbV0M5R0BmZjX6pv4RXqj7UMjZvSOMmlWc3+fb79wAAs1zyOfLlBV7w6eIfXeqt0tfaH3VvY1tAZHOXdI24qzazRj8ZlJqFyvK7nh74S2rL1HuWzknsEHZ8J+/O429G4W438LDUh02ynhM/pGpHUEU0GKvm57B0z+eXvlhrwzd/L2vA7INr8Vm5Osds9J6eymn8ydNYW9noqWpdVjE/lM6exxhld7z7v9Hh0zIYjK8OZEJS5zTwg37aa7dyegcVkbu42L14LMbpYNRYytFBXyEDnuMbw2J74+Q3hrnMIaS0kNbz0gNwQEBAQEBAQdPL4yrmsXbxF5hdWvQSVpmg8Ese0tdx+oqPLjjLSaW7T0Zidp3azprC7g4f3HEX81hrOLx7WxCw2tKLdiJrelgcC7oY7sOXDnnjsBytTBj1OOK47Wiax59d5j9GZ2nqx2/e3Ob3a2rzO2uGzcWJZqMQY/JWnhxd+TXzM99jj6fKSSuJY2k9gX8nyW9G/m8tJo+FbT2h91tEbnbV3ruLfgYruJzFXJZi/kW3cPYhHFeI2JZPZGOeKvI0N4aQ1wPpxkbp4htsshvLsprHa3F5OtjrepsVLj4rVhjnxROdx85zW9yO3og3rH4+rQgbHXrQxHpaHGOMM6iBx6IIo332w3E1lqbbvXW2WV05WzOgstdyLYM9HYdWsts0JqhaTAQ8ECYuHoeBz8EEgaEGvG6crDcl2Bdn+qT3k4MTin09Z6Oj25MnPTxzz688dkGwkcjhBDT9uN29Eam1Rd2n1FpY4fV2RdmZqufq2HSY2/JGxk0kLoHASxvMbX+zf08O6uHcO7R8tomeVtTnx5K1jLE716dPOGWo7MtwexOY2hxmcdZuZfFZSvYytqPgz3rwldNZexvkDLM53SD2bwAeyzSOWNkefNOfJN5bDtPodu3e2+lNFzOqz28BgsfibFqvD7Ns769dkTngHvwSwkA9+69oW3oNKzGm9Xx6zn1Ppm5hmMt42ChKy/FK8sMckjw5oYQD/nPIkeSrsmDPGe2XFMdYiOu/l8nvmiaxWWZ0jpoaXxRpyXn3bVixNcuWnsDDNYlcXPd0js0cngNHkAB3WzpsHu9OXfeZneZ+MsXtzzuj7U2xNbWG+VjcbUbsbkdOXtAWtF3cPYrl7p/bXmWHPJPzejoaW8efJBWw8s/sXofVW2m1eA0BrDUseoL2nq7sdFk2tc11ipFI5tUydXcyiARNeeSC5riPNBourdtd98RvVqLdPaLK6CdBqXAYnD2a2o4rxfC+jLce1zDXIBa73w889/m9vXkJwq+9mpH757I2fZt9r7PkM6+PndPPfp554578IKr2fAnhs3tFncTqDU99+5eetXdQzahq5fIx0Y8/LZNmC02iJhCWQyNgaAY+S2IevdBaTEtyTcVUbmX135AQR+9urAiIzdI6ywO79PVzxz344QVa1p4Ls5qzQseAp6/iw+abrjUWadk6kL+p2Czdif3/AB/B7h768wb1DsJImOHHAICX9vdnDoPd7XOvKNmlFhtS4fT2Jx2NrxOa6kzGx2WEEn5paROwNA8g08oM7ubX3dkp0Z9o72km2opne+1NR17LoLMJb2DJa7uqJ7XAHux4cCRwPNBgvDrtBd2Z0PewWVydC3ks1ncjqK9HjKrquOqT3JjI6vThcXOjgZ2DQTyT1OPBdwAlJAQEBAQEBAQEBAQEBAQEBBwgEc+qMTG7lGRAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQf/Z"
	              className='rounded'
	                    height='70'
	                    width='150'
	                    loading='lazy'
	               />Cafè  of Dreams</small>
	            </MDBNavbarBrand>
	            	<Navbar.Toggle className="" aria-controls="basic-navbar-nav"  />
	            <MDBNavbarToggler
	              aria-controls='navbarSupportedContent'
	              aria-label='Toggle navigation'
	              onClick={() => setShowNav(!showNav)}
	            >
	              <MDBIcon icon='bars' fas />
	            </MDBNavbarToggler>

	            <MDBCollapse  navbar show={showNav}>
	              <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>
	                <MDBNavbarItem>
	                	
	                  {(user.isAdmin && user.id !== null)
	                  	?
	                  	<MDBNavbarLink active aria-current='page' href='/home' >
	                    Home

	                  </MDBNavbarLink>

	                  	:
	                  	<MDBNavbarLink active aria-current='page' href='/' >
	                    Home
	                  </MDBNavbarLink>
	                	}
	                </MDBNavbarItem>

	                 {

        	            	(user.isAdmin)
        	            	?
        	            	<Nav.Link as={ NavLink } to="/admin" end>Admin Dashboard</Nav.Link>
        	            	:
        	           		<Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
        	            }

	              </MDBNavbarNav>
	            </MDBCollapse>

	            <div className='d-flex align-items-center me-2 me-lg-3'>
	              	{(user.id !== null && !user.isAdmin)?
	              				 <Navbar className="p-0" >
	              		      	<MDBBtn className=' p-0' color="light">
	              		      	 <img onClick={toggleModal} src='https://static.thenounproject.com/png/5641-200.png'
	              		      	 className='rounded-circle' 
	              		      		height='50'	              		 
	              		      		loading='lazy'/>
	              		      		
	              		        {/*<span className="icon-button-text-right">{cartItems}</span>*/}
	              		      	</MDBBtn>
	              		       
	              		      
	              		      <Modal isOpen={modal} toggle={toggleModal}>
	              		      <ModalHeader toggle={toggleModal}>Cart</ModalHeader>
	              		        <ModalBody>
	              		          <Cart />
	              		        </ModalBody>
	              		      </Modal>
	              		    </Navbar>	
	              			
	              		:
	              			<Nav.Link></Nav.Link> 
	              	}

	              <MDBDropdown>

	              	{(user.id !== null)
	              		?
	              		
	              		<MDBDropdownToggle style={{ cursor: 'pointer' }} tag='a' className='text-reset me-3 hidden-arrow mx-2'>
	                  <img
	                    src='data:image/webp;base64,UklGRlIYAABXRUJQVlA4IEYYAABQoACdASoYAg8CPm02mUkkIyKhIpIpAIANiWdu+F9JAzX4odB8GMS9CP3v+J5pHwCLRAR6V84vpL/T3sAd6B77PMP5ynpJ/wu+x+gx0yeQ5+N/9B/cO5X/NdI14T9sOX7EX+Wfd39x+XHsV/t/lm9kfl3qC/j/9H/zu8igA/TP6f/u/8B+6fwU/SeZn8f/lPYC/Vf/g8a15p7AH8l/uX/g/yXsN/+H+x/LP29fo3+o/9v+q+BD+ff3j/u/4P20PYb+3v//9079yhMiHhEA88Ysv33Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4Kk2uArO6F7gvcF7gvcFRg0vNy1103oeHnEpVJUuXZyxFU/TObtWXHTuYpgLlwgsA/gJN2IlK7ZmI3ZBIu7pkU/pFbytGkdBj7pl0LqkeMN0scmGOvOnawckEf3RCh4OsZOZnyxI71jg1kQ4Be3ZBF55XLscUODlpaALaN//HA0RdziIB3Kvl0tCrZOMcaBpFEtDQMPBEZybi8xEDNz1bWWeOQzVhdyRXyaw3v/com/LcmtBuIJF5I2XNTXZb5U1XKiCZmDXsQ7k7bLPdGK8J9wAGoHE/eRRZcT8k3ihcZf7MdGakmViVz8RQw/Ype579y47NqWDZe35FOGvFrbzUbIoLimb1PFeak+19JBDN12Nboth2rdxH0DRVZa/3dQKLcI18jNaxSSGVTrwiIBVvSCXi53WFT8bFGzhDO7vf2jLIsFpfXQBbEOyEUZ/mo43M4qRg4xqZM9ME6Q5eDcyFdMFDje0pt6+4MTh0MDpwF5bYHoTZLCTYy4LVukkFcBt4Lzt+7IMxYUF1/0uA2sxg8ColqodsqkXq0xdtRR4AsNxE1GmcFYfM+JQ0zpSKX1YrVrjI1z5tkWHd531R+w4CsdBNvTV+kSmlK050rmCtHj7bwtP+cJAY/LV4FrMgmbLm/IQH7kzkXQ5Qdn9wPa7aCdx29oBo/ztUyP8ryLIeEQYANoNyUU76zTQfWrbB9K8cLJckfuEWQ8Ifse31IGBfUAs1D1jgUYA1G0LuMm9fW5j5dnKREJVZ1CBTzBZOQtWVa4BH5kcCTLi2smB8dfUimoEVHhcxfqC+2/sxPwL9wXg9U7/xyjUoaVJpQgGgAnP3kXpvIKpPDwPaMDxsEiw00zetVi0+MgZm5Rst4qaujOCba69DxtrTlCQvj820TiY9MDE/ldV4on6vG2JOjYti/cyjVKKuGlJgBhE40bSgiLdKLId7U8gh8oSL0ukYK3VuDmTtQzOnzVxt2pqyVdhRRSj0tUBac3/QLxsB6bsT9NfP4Lg6HW1Ny07EA88h1Ys84iAiEuh9cr0NRwYsv33Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be4L3Be0AAAP7/ncgAAAAAAAAAAAAAAAAAAAAAZ2MWQPlxk4u8MH80/xZ0AHMidWSOOiPV88yA8BbHR4RG04w8AiFI2C09V0rv6gYmvi1Z4sCPHNuM3D6ltM1VpuuR3bJ40MiRjUy6LVK50m9MHhNKGrLHZ9IJ8GoFPgWRbU+nCCs8tzsj02AkKRn6gPGrU0Nhrfo7HyVE/KtlKgFLkaloDlGZdn4rgoR34tbJl0nin9xLBPvQXTwW3QZA59/uspuQC8trwkrJAn2aJ/oeD9estljIRRv7kgwIarsgCCx15S8/N+W4QLYr/fg5IAyyqSeSQ3IbNBsdNd4JcRvxajOz/2ECKXEN5A1KMwLqAmZ+963hwdvYBq+oC5c0tcEpFIJBd/QgFX+1qEWqKBQrh3MI046zVFCSPD4kYS1V0W1relOhw28mUu1ykBoSigSdypkDKMqNpWNpqpQ/6kNpewAuriif37ArDFfrN1vNwd7BbBL8d7ncwJu0ZHVvRKIQFbFI0W480CEz2mYliaWGwOKagmrTp8dMwdtaLONM0VWkD4D4XlsikYaOD+cP+wWAC9+XDbZ6+V0kan4gpnc0YZAee6K6hVmwrASKOWq8WQULohr+ahu/gbvhGjEQuPjpCu6Y+e7fWh93zPx3gtLq+co5KDwDfx3RFzOO0plZi1P4rIL1gT22/gZ04q2Klsjzcyp74LfOh3CGSBSEQWox51bWaIACeaGHB3a0odsZVjkRP5LM09RvozkOGo6eNAxjve4wa7QKAT4AA+wpjefuuYwYTklAzmCQGcj31K2jhKyuoLbPZ4HApnv+V+m60TZj3aFQjTjDLzrz3BmWFjpBtKz7CPyJv9+sEP3dzYP7WgUI6h59EpQL7Flim7i2QyMXNDdscGeeMbA+lZ/dm/jR8U4oD1uRw2zFX/GZCxw3hxSXO0PWgh1Bz8RslsxUgB9+IftT5dU6udsG9s4I3H4xynofLW8gO+649XtTwfu2Tur/awNWz7fK4RLbJg0G/knwb/SBoweYfwfP3D5qhIuei8xiu7wLfz4ea5/upsVSxANLyl3UHcFvMK/h5hyGjReiNRx7mniZ9TLg5tcQFz+ztp9e/cKTC68Pw7h9JHUB+tGLCC4a8oOrdVGLcuSDMte2bWS46J6loBvROvJJetrgxhbooXKm8Ao/yfwyOyZk4noUn48+SVcA5H+bBjl/pnCO9RL53t4RmWVZ/xuTu5zUzl/f9bDIOpj6r4LQst5tfOiZ1E18IaoDyzKXi18iD5gfFvGoYg4MeMYPaKnUpiS3zoabxkR+Zhgh94OqXKqEg02jWa0yrcJk+soN9JHRj5M6ENRHcV6ANPGnuM5seEEwFlNYIP11dbsnalIb9j7fsTLlH4GMgOuJX+AHYbdAoMZlX69gqQIaR17V889059xgt26l0AyQZjnrZQYxfLRf3DHQi4WUu7BiqmhBFAuii0l6I7kgn0xVSSnme39VSqiMJR+lyY7H2d1uGK+fzM5YZtEGDoBffyX8vCW3oEN8k48+ykEv9hklcEWlUjgB5nNK5uThLixJlflSAFESVsex+29BmefFflBYL21qStjY2NuMbuGyiIOn9Y2kj7qEGKVjLXYb0jEMdyq7dkU5JYTdMuu5Q07mDBbhZHAlxOqRAf4qLttxLgcVl5l0j7bnfH1Zi2t57mgVKX7aQdYRGnrigfLCMxrs5OnuB8JzTKwRSzwo6ttpHi/iaNYQdOqVxTR+swEw/Hdb25l6dS1W33qoXQlOcH0Hv2UnNIPQkb8datHdrz5gwfEK8SK87bWcyNmgqRxlprTyqTxTyJCLSsnsCycBpTzBRQnHMCn/JkADhx47zwZqZmYxt/EaU+J8YQQlFIhP351uVVlSqfqwzz6rkVZQaXUI2ZuvQTiEhDvAsEHuXEVmweZ0obTqc7sskwQ8ppVajVwZ7cJFfi6JCcDsC4egoJkDuZ1lwKpDYasl+Nt0Krd94dAVlgLAYc2+5cPkLtX1pHQfC0FmrkQAWg5f8phgq8BE9AWAi2zrgxP/hNvUlgyCV9bdsKH7OYd5GvrPEAS4mBAx8M0wiBEcPKdCgsns0W3fO0UdKGgvxw234K+UHLDBMbPZ4IrqlRCDxy8YJtqzg+JAQztBvG5AKlrEVJZp/rL10IZt5CSAS4d2ion6f7MmT51AZYCwDSyhuRSIzT1KXj+SKWND79NXFJxoGNWua+3VSa2OsMGbb1UhkR36ma4/AnDjYG5hyY/F8SG6Zrq/j4/BBOQj34031IOMPS5v9ewxWpU2oCaEO2J/YRQdnnWvJEuqTIZajTeN0VESFCs9rCoKCM+0yd804gUDaQelcEZd8nRER2WaXX5xI9aCp029v8iQ/E8zJJOZ9lM5TngWKtdLy2bu5li8EqqTdRzOHISlrC3rn6lKySaosskYHvY28gF6ir0Ee8x+2/smY4XeCRq1rhSYXbvgvUAIaQS8zZZqQm+8OT5ollAda0bStG5ccnnv0LvDrXkzcgE+IyOXH5o8uo3MKcwoSwF7+dbyy2ji/F20JTUx1ySChOd6dxo+RKFhh5ZEGwdnI7Nxa9JVGl3AOLSDVjoJKxTsImas9+chrKJhcGkgXnNuvz2kmHvQv6hKoKYfcpS6KBcZwfy+tf4JRZD4G7rARHy2iKjTSyFz0jbe3C320UCRY8jzeueL3jhbNkROJ8eT+NFaQTPSf0GabvaGAPyrdKn9eFKFe8+/PXE7bvazfX2hsn0imSPamS31P1W6IJ0FnkRG9wsttQztqt2+rnlPTBl6uZnk3HJVU3Pm5qq9Zj93jYkZYT8Ma8ab0rMNHPrXaDnYWn5njpI/T/d8Z6Zd84tY916QkkyhHuheUT0pcbjBZxc29Ck/gBQsPP1FcU+ZhYVPpw/tf4iBY8UOA4dYHXflAJDsL54OetahY2iinmV6nkkXC5/P680gMukYoOPfqQDqzA9864aaVWGJuWwNJwUB0lw1qKbOL2TFAAzELVRzW7xM/GV2hoaMmUdrX4uh5+jlmu16kMFSnN/VzNHcnRf8JAKXT8UwpwbDWtvyofBg9La5goCU854kt+TKcgYeAfeiP4lQf5J3f+p6WRh02jLAvjLVpE5frZE242GPOm1R46hSswywMlWkc2PhY2ZjsioibRqdr+jN2By/g0H5c5mZFcDVM1K2FIFeNRbTvW5+oXTa6ptBIo1tSoyJoMStZ0LgH2zb9SLG5TnpkqKUOZQ8UrxKThfakRNzi6TdNqcevqweye96nYeC4mK/GtazJfEs5t6HkpVeAPDOkfOYJ9XTLPhP1dYBib5xhh08ByOF6rOpJWOhLyhP/TRZ6rnhB6KE+KVOsA+HP5r/Pwor2W+W6ymF7TzS+VSGI1ufvhXna2QhW0x2akblQ6expz1wVTJiLAX4649wpxv+X/L/l/Cfl+vHmDoSk5d7WJ0MjFi69BCVf2zL620vJkdCjtVmNHYk9gJRW7Vid/EN050REerjnke7O82k6AosonE7CmF1xyiRkLSQvN9tdJGez49E3B3WqNCIQo5b2ODD57rQ89y4Kh2u3Ay/2cI205goe/D2zED0JeGSoSDk25pVXntFprcyy+1FPPBsbMMRLH0oXJ9C8VkzD1Of7nqvdeVmE2rp8sDQDw5wNmXi5LIA74Ucb75/KfB3rXKs7V10rfhT0eveE95R9zBVjGsNJaI2NG0LJDKB6E8t7gqOws0jm7/A8JtuCN+S9NxLgdm/mpQiWDVWGlmGzo+sGYsvYqeVTw9XSJCalZ7tzDq3J/bLgBglnEkx/2duxXXp90GYOe0XpKse8XV7FWTALuGtoZ/USZ6z7cQlqcre7t87N1Y+1+qjE8d1fsrCVxT8koJM0H344Nvhg34Uh2nj9MpOw4MqjzsnMNEnxuK9BXgqXcpr1F0Oihdngm4q6Jgb2ZFBZsqSga+66PVd5tmRdfavJtksPBK2h6kl9a+XD2DvO0E2ADnKDy89wbHq46GKEe1frvXXOUOr2KVLL1VDhGRzpLnb9AMvonF01uw+LXDv+N6ULXQeDdFwYKvYcWW1X6wJPKLYg+x8gEaSNxxeOu06SDcelyI9zp0BcH9g6gjonAIYWX5msG1+KIVQIW4nGmTOU0jNWZdjZfvPEDyzrTTqLtF6ktnRV6CiIC3vgpsO+aV+0lGb0nIMgt3SFrdd44BvHSbzMkRhfhkjR7S3ph/l1KSE56fbec8xgsoE06Ao3TkQUAAAAFhJK9OAYeyO9kUjWBqto9mjdoz9jLkX9qwflqkCfQV3Zx+s+k4N0gU0lDz9biUdLQUPJIROqixJTlo1gAAAAxsZ2y3Pp0vzLwW2Z6G61A25yziLqG9IPwag19R3FDeRBYL63fMXUh2dbUY3NzY1PGGa9Pll0dVwu8GOraqkV5fykwUgDllMzr+Wx19h5L60FkBbcp2VgqLuQMt8j8+OE5u49BtWEsr/xquZmN9wJrq9NL6cHWrKzOMRQZ3Eu9SEjory0iX0ivB9PofGJYXQLR+KvIymN86J5/iwAAOwZ81+rjn4472W91n4rC/bYksZHbNzLkFZ010nKHR+GrdbDx1C+qIwo/xrWlNc1KDJ1BgBrWKRirTbOJcbgax9NzqdU7yI7NP8WYnhSvi3P2rNR2h2JDJQdlFjckRjWsyspWmeBcCZTOCqXPw7IuM95P48996I8f5oUmQHMaBC+vIxzfdsPfYiuvWk/IH0Hb3xXJyToA7xlnF5rq5KpoY29f48q6TWqdZ1ovP/tEKuNB/On9x+j7riUwOaElPBRWXPHa/xX/edVmECjipBCf3vi0ze7KC/6nCGf/0zbPYf6shyNTayHE4AwnwmdZF0XGFcxhpEuKs6d/RcEU/VH8aOfiefJ2ohYdKPUY8lf7ZRPXEB+BYt5BmR/PO+kKzniZr7c3jEqaox8YfrV8YtZNA8lxfFxbki+ZCBQSxT5Lhvsva+h8cdKdbj6FEj5ltwVAevtcYYoby4E3To44Of2dXZSzZa8ZNx6O1s55J203Yp+n29O+Z8jj901BkVSQO8r3EG3RTlkiTjE9Vu94lfl7qS9rVOLMXOR7G00tj3yTTbprbxfIeNchZnEf6uVUCUDt04EKI+Y4LywfXomDXlLqZ6/FUHYt5SFNptQIHltJ2C9aNUizWMGM/7wW34oXoI4cIKBx+AmKbA3ONN7zY6IDechGAX3tjS1wRLbyY0Rst9g874FKuxhskgDYfd7GnygGFD/tZ1BaPS3PrqcRT9qvC4MhW8Ka/OUtg1X/Y6GH+Ixa4uVylFSJ+HdbztIZnM7Ab9op3RGzhy5Gg1h5SnBJlC/TAPb7g9avFvYc4mmOvEw4wE+vu5hLX0rBnVQ0nKxgd+CfxeMicKyTySqFB9OrYqiExWv7uS2oVNk2XYkVT8lR+pa9JKtHdpLCaf6vy5cOiPC/uy5oVh0KdODYwm6C5zbvmyIHIM6B+6KiNZ3OCBkD4YRdIw/pWi4tGop9srKGjlfCC+5L73J1BJGSFLz9h+q5BTobJBLdojSHB6U9CLTVKRITxLhNiZAvs5yYTw8M1AnCTglDqe4am8xfnHNu/NKlfNseeVwx4wbB8Zs1KHVIs9SOB1Pvy2GQRnprr7yVz0l92m6vqHBYOOuaTtP5mVHddaJIdfk3Zkne2cJNgHQVNw4ReRY2tRMBKT8Z0CxKBTydpyR6NDY3GW+J45MqydvlKRrzos72/m4IoXm1bthjZ9nZvL/aYYRn6xRScGjMnEh1U7vqI8eOl5F5cef+UO0tFVa91rIUmULnhZC/OnxDiT94fzkiYV/HXto+jAfW59zLJLSHvdpL6L9Nq5p/rAhpad/hTCj4+wPqYTXxwAL/x8SXVX7/Me//E04dTx+wRRLOXMx/rLX5JNrRa46RdHCMb3zKkurYNLG4Flj8ZRA2K/6T+gN/+h13zIJlc4O+KLp1s/yhQwqUnMQsGs4/6AvBDv9W/cIwTBl2PTCDBgNonMprU/hv6fTvowxOUdLNN87ftD5B7+nYSVTIV1xddWnyoyHcZhi/1ciNIv5V8gZmzqh0s3YRsX4Ia/tFyjFulgpw3efuhh+65xfx/XzDTcJWuDIW1HFj4Uw+1fr4fzfb6z2pUWBPZRezrjVi6id/5EmEfgAc/OlHAP5v+f4mZaVHGHeaP/EBe9cmJ9pzJg3+0TyGMaQFbHPbs97NMOyzVXYeS08s57/mhEoxXGz0AoZebGnUJK4hSTDomF+wnQ2HbXIKjofJ2TVLqyYIAkXkJ3pAZL8vK/7mi/F+y8p3r/rNWNzO3oYT81WwFLPf0wM67FNPqhHH7A06zQFTo3dck4E/l9kOJSdtz7lkh3ubZZBLc3dB3C/RJUBz+BJl4DcZ09jhr/oA416FIR7x3jKwV21E3U+siSYp01PUCURXdy6GTwvOxCZdLvo5t7tUfQO0Rm7VX+yShn1YIR0cHo3jGcpzTBO/OkblRXvwTJobKnPeRf6I7yrIuCjQTDvGw1Ao2A6Gn91CAGkUlXKkWi6XMjdgV+4GnpCrcOlJBDAWV4wde0knElAUwAAAAAAAAAAAAAAAAAAAAAAAAA'
	                    className='rounded-circle'
	                    height='40'
	                    
	                    loading='lazy'
	                  />
	                </MDBDropdownToggle>

	              		:

	              		<Nav.Link></Nav.Link>

	              	}
	                
	                <MDBDropdownMenu>
	                 
	                  <MDBDropdownItem>
	                  	{
	                  		(user.id !== null)
	                  		?
	                  		<Fragment>

	                  		<Nav.Link as={ NavLink } to="/user" >User Profile</Nav.Link>
	                  		<Nav.Link as={ NavLink } to="/logout" >Logout</Nav.Link>
	                  		
	                  		</Fragment>
	                  		:
	                  		<Nav.Link></Nav.Link> 
	                  	}

	                  	

	                  </MDBDropdownItem> 
	                </MDBDropdownMenu>
	              </MDBDropdown>
	            </div>
	          </MDBContainer>
	        </MDBNavbar>
	      </section>
	     // </MDBContainer>

	
	
	)
}