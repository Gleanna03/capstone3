
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function OrderCard({orderProp}) {

  console.log(orderProp);

  // Deconstructs the cart properties into their own variables
  const {_id, firstName, lastName, email, mobileNumber, orders} = orderProp;

   
    return (
       
            <Col>
                <Card>
                    <Card.Body className="mb-3">

                        <Card.Subtitle>First Name:</Card.Subtitle>
                        <Card.Text>{firstName}</Card.Text>
                        <Card.Subtitle>Last Name:</Card.Subtitle>
                        <Card.Text>{lastName}</Card.Text>
                        <Card.Subtitle>Product Name: </Card.Subtitle>
                        <Card.Text>{orders.map((order) => order.productName)}</Card.Text>
                        <Card.Subtitle>Quantity: </Card.Subtitle>
                        <Card.Text>{orders.map((order) => order.quantity)}</Card.Text>
                        
                        
                       
                        
                    </Card.Body>
                </Card>
            </Col>

        
    )
}


