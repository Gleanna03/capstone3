import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import AfterLoginHomePage from './pages/AfterLoginHomePage';
import AdminDashboard from "./pages/AdminDashboard";
import Product from './pages/Product';
import Order from './pages/Order';
import ContactMe from './pages/ContactMe';
import UserProfile from './pages/UserProfile';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import CartItems from './pages/CartItems';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { UserProvider } from './UserContext';
import CartContext from './CartContext';



function App() {

    // Global State hook for cart
       const [cartItems, setCartItems] = useState([]);

       const onAdd = (product) => {
           const exist = cartItems.find((x) => x.id === product.id);
           if (exist) {
             setCartItems(
               cartItems.map((x) =>
                 x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
               )
             );
           } else {
             setCartItems([...cartItems, { ...product, qty: 1 }]);
           }
         };
         const onRemove = (product) => {
           const exist = cartItems.find((x) => x.id === product.id);
           if (exist.qty === 1) {
             setCartItems(cartItems.filter((x) => x.id !== product.id));
           } else {
             setCartItems(
               cartItems.map((x) =>
                 x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
               )
             );
           }
         };


    // End of Global State hook for cart


    // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear();
    }

    // Used to check if the user info is properly stored upon login and if the localStorage is cleared upon logout
    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user])

    useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
         
         if(data._id !== undefined) {

            // Sets the use state values with the user details upon successful login
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });

          // Sets the user state to the initial value 
         } else {
            setUser({
                id: null,
                isAdmin: null
            });
         }
          
      });

    }, []);

    return (
      // We store information in the context by providing the information using the "UserProvider" component and passing the information via the "value" prop.
      // All the information inside the value prop will be accessible to pages/components wrapped around with the UserProvider.
      // Wrapping all components in the app within UserProvider allows re-rendering when the "value" prop changes
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <Container fluid>
            <AppNavbar/>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/home" element={<AfterLoginHomePage />} />
              <Route path="/order" element={<Order />} />
              <Route path="/contact" element={<ContactMe />} />
              <Route path="/user" element={<UserProfile />} />
              <Route path="/admin" element={<AdminDashboard />} />
              <Route path="/products" element={<Product />} onAdd={onAdd} />
              <Route path="/products/:productId" element={<ProductView />} onAdd={onAdd} />
              <Route path="/register" element={<Register />} />
              {/*<Route path="/cart" element={<CartItems />}
                onAdd={onAdd}
                onRemove={onRemove} />*/}
              <Route path="/logout" element={<Logout />} />
              {/* "*" is used to render paths that are not found in our routing system.*/}
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    );
}

export default App;
